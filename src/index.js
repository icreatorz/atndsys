import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter as Router, Route} from "react-router-dom";
import './index.css';
import Login from './pages/login/login';
import * as serviceWorker from './serviceWorker';
import Content from "./pages/content/content";
import * as Sentry from '@sentry/browser';

Sentry.init({dsn: "https://2bf0f93cf81b46c4b4809f8562c9fa4c@sentry.io/1436953"});

ReactDOM.render(
    <Router>
        <Route exact path="/" component={() => <Login/>}/>
        <Route path="/dashboard" component={() => <Content page="dashboard" title="Dashboard"/>}/>
        <Route path="/member" render={(props) => <Content page="member" title="Member Overview" index={props.location.state}/>}/>
        <Route path="/activity" render={(props) => <Content page="activity" title="Activity Overview" index={props.location.state}/>}/>
        <Route path="/mark" render={(props) => <Content page="mark" title="Mark Attendance" index={props.location.state}/>}/>
    </Router>, document.getElementById('root'));

serviceWorker.unregister();
